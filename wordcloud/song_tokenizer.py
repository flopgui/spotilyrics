import langid.langid as langidl
# from nltk.tokenize import wordpunct_tokenize
# from nltk.corpus import stopwords
# from nltk.stem import WordNetLemmatizer
import spacy
from collections import Counter, defaultdict


# SUPPORTED_LANGUAGES = {
#     'ar': 'arabic',
#     'az': 'azerbaijani',
#     'bn': 'bengali',
#     'da': 'danish',
#     'nl': 'dutch',
#     'en': 'english',
#     'fi': 'finnish',
#     'fr': 'french',
#     'de': 'german',
#     'el': 'greek',
#     'hu': 'hungarian',
#     'id': 'indonesian',
#     'it': 'italian',
#     'kk': 'kazakh',
#     'ne': 'nepali',
#     'no': 'norwegian',
#     'pt': 'portuguese',
#     'ro': 'romanian',
#     'ru': 'russian',
#     'sl': 'slovene',
#     'es': 'spanish',
#     'sv': 'swedish',
#     'tg': 'tajik',
#     'tk': 'turkish',
# }

LANGUAGE_PIPELINE_NAMES = {
    "ca": "ca_core_news_sm",
    "en": "en_core_web_sm",
    "es": "es_core_news_sm",
}


def tokenize_song_n(t, language):
    # language = langid.classify(t)
    nlps = {}
    if language not in nlps:
        if language in LANGUAGE_PIPELINE_NAMES:
            nlps[language] = spacy.load(LANGUAGE_PIPELINE_NAMES[language], exclude=['parser', 'ner', 'textcat', 'custom'])
        else:
            return None
    doc = nlps[language](t.lower())
    doc = [d for d in doc if d.pos_ in ["NOUN", "VERB", "ADJ", "PROPN"] and not d.is_stop]
    return doc


def tokenize_song(t, nlps, language_identifier):
    # language = langid.classify(t)
    language = language_identifier.classify(t)
    #print(language)
    if language[1] < 0.5: return
    language = language[0]
    if language not in nlps:
        if language in LANGUAGE_PIPELINE_NAMES:
            nlps[language] = spacy.load(LANGUAGE_PIPELINE_NAMES[language], exclude=['parser', 'ner', 'textcat', 'custom'])
        else:
            return None

    # nlp = spacy.load(language+"_core_news_sm", exclude=['parser', 'ner', 'textcat', 'custom'])
    doc = nlps[language](t.lower())
    # print([d for d in doc if d.pos_ in ["NOUN", "VERB", "ADJ", "PROPN"]])
    # print([d for d in doc if d.pos_ in ["ADV", "ADP", "PRON", "DET", "CCONJ", "AUX"]])
    # print([d for d in doc if d.pos_ in ["SPACE", "PUNCT", "NUM"]])
    # print([(d, d.pos_) for d in doc if d.pos_ not in ["NOUN", "VERB", "ADJ", "ADV", "ADP", "PRON", "DET", "SPACE", "PUNCT", "CCONJ", "AUX", "PROPN", "NUM"]])

    doc = [d for d in doc if d.pos_ in ["NOUN", "VERB", "ADJ", "PROPN"] and not d.is_stop]
    return doc
    print([d.lemma_ for d in doc])
    print(set([d.lemma_ for d in doc]))

    # tokens = wordpunct_tokenize(t)
    # tokens = [t.lower() for t in tokens if len(t) > 1]
    # tokens = [t for t in tokens if t not in stopwords.words(SUPPORTED_LANGUAGES[language])]
    # print(tokens)

def count_words(songs):
    counter = Counter()
    representatives = {}
    for tok in songs:
        if tok is None:
            continue
        lemmas = set(tok['lemmas'])
        counter.update(lemmas)
        for l in lemmas:
            if l not in representatives:
                representatives[l] = Counter()
            representatives[l].update(tok['representatives'][l])
    return counter, representatives




