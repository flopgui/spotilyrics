# Generated by Django 4.0.1 on 2022-01-24 00:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sessions', '0001_initial'),
        ('wordcloud', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usersession',
            name='session',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='sessions.session'),
        ),
    ]
