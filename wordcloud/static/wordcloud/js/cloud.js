

function updateProgress(url) {
    fetch(url).then((response) => {
        response.json().then((data) => {
            let el = document.getElementById('progress_count');
            el.innerHTML = data + '%';
            if (data != '100')
                setTimeout(updateProgress, 2000, progressUrl);
            else
                drawCloud(resultUrl);
        })
    })
}

function drawCloud(url) {
    fetch(url).then((response) => {return response.json()}).then((data) => {
        console.log(data);
        // let lemmas = Object.entries(data.data.lemmas).sort(([,a], [,b]) => b-a);
        let toDraw = [];
        for (let lemma of data.data.lemmas) {
            if (lemma[1] < 3) continue;
            let reps = data.data.representatives[lemma[0]];
            let rep = Object.keys(reps).reduce((a, b) => (reps[a] > reps[b] || (reps[a] == reps[b] && a.length < b.length) ) ? a : b);
            toDraw.push({text: rep.replace('"',''), size: Math.pow(lemma[1],1.3)*2});
        }
        console.log(toDraw);
        var cloud = wordCloud('#cloud');
        cloud.update(toDraw);
        // wordCloud(toDraw);
    })
}


// Encapsulate the word cloud functionality
function wordCloud(selector) {

    // var fill = d3.scale.category20();
    var fill = ["#84d2c5","#e4c988","#c27664","#b05a7a"];

    //Construct the word cloud's SVG element
    var svg = d3.select(selector).append("svg")
        .attr("width", 800)
        .attr("height", 500)
        .append("g")
        .attr("transform", "translate(400,250)");

    //Draw the word cloud
    function draw(words) {
        var cloud = svg.selectAll("g text")
                        .data(words, function(d) { return d.text; })

        //Entering words
        cloud.enter()
            .append("text")
            .style("font-family", "Arbutus Slab")
            // .style("fill", function(d, i) { return fill(i); })
            .style("fill", function(d, i) { return fill[Math.floor(Math.random()*fill.length)]; })
            .attr("text-anchor", "middle")
            .attr('font-size', 1)
            .text(function(d) { return d.text; });

        //Entering and existing words
        cloud
            .transition()
                .duration(600)
                .style("font-size", function(d) { return d.size + "px"; })
                .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .style("fill-opacity", 1);

        //Exiting words
        cloud.exit()
            .transition()
                .duration(200)
                .style('fill-opacity', 1e-6)
                .attr('font-size', 1)
                .remove();
    }


    //Use the module pattern to encapsulate the visualisation code. We'll
    // expose only the parts that need to be public.
    return {
        //Recompute the word cloud for a new set of words. This method will
        // asycnhronously call draw when the layout has been computed.
        //The outside world will need to call this function, so make it part
        // of the wordCloud return value.
        update: function(words) {
            d3.layout.cloud().size([800, 500])
                .words(words)
                .padding(5)
                // .rotate(function() { return Math.round(Math.random() * 6)/6 * 90 - 45; })
                .rotate(function() { return 0; })
                .font("Arbutus Slab")
                .fontSize(function(d) { return d.size; })
                .on("end", draw)
                .start();
        }
    }

}

var pdata = JSON.parse(document.getElementById('pdata').textContent);
var progressUrl = pdata['progress_url'];
var resultUrl = pdata['result_url'];
updateProgress(progressUrl);
