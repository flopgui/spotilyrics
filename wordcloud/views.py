from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.shortcuts import render, redirect, get_object_or_404
from urllib.parse import urlencode
import base64
import os
import random
import requests
import string
import json
from celery.result import AsyncResult, GroupResult, result_from_tuple
# from django_celery_results.models import TaskResult
from datetime import timedelta
from django.conf import settings
from .tasks import process_all_songs
from .models import CloudTask


SPOTIFY_API_URL = 'https://api.spotify.com/v1/'


def index(request):
    logged_in = False
    if 'access_token' in request.session:
        logged_in = True
    context = {'logged_in': logged_in, 'display_name': request.session['display_name'] if logged_in else None}
    return render(request, 'wordcloud/index.html', context)


def login(request):
    state = ''.join(random.choice(string.ascii_letters) for _ in range(16))
    query_string = urlencode({
        'response_type': 'code',
        'client_id': settings.SPOTIFY_CLIENT_ID,
        'scope': 'playlist-read-collaborative playlist-read-private user-top-read',
        'redirect_uri': settings.SPOTIFY_REDIRECT_URI,
        'state': state
        })
    return redirect('https://accounts.spotify.com/authorize?'+query_string)


def spotify_callback(request):
    if 'error' in request.GET:
        return HttpResponse("Authentication failed. Please <a href='../login'>try again</a>.")
    url = 'https://accounts.spotify.com/api/token?' + urlencode({
        'grant_type': 'authorization_code',
        'code': request.GET['code'],
        'redirect_uri': settings.SPOTIFY_REDIRECT_URI
    })
    headers = {
        'Authorization': 'Basic ' + base64.b64encode((settings.SPOTIFY_CLIENT_ID + ':' + settings.SPOTIFY_CLIENT_SECRET).encode('ascii')).decode('ascii'),
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    resp = requests.post(url, headers=headers).json()
    user_data = requests.get(SPOTIFY_API_URL + 'me', headers={'Authorization': 'Bearer '+resp['access_token']}).json()
    request.session['access_token'] = resp['access_token']
    request.session['refresh_token'] = resp['refresh_token']
    request.session['display_name'] = user_data['display_name']
    request.session.set_expiry(30*60)
    return redirect('..')


def cloud(request, task_id):
    context = {'pdata': {'progress_url': f'../cloud_progress/{task_id}', 'result_url': f'../get_cloud/{task_id}'
}}
    return render(request, 'wordcloud/cloud.html', context)


def cloud_progress(request, task_id):
    res = AsyncResult(task_id)
    if res.ready():
        return HttpResponse('100')
    else:
        task_info = json.loads(get_object_or_404(CloudTask, task_id=task_id).celery_info)
        #res = GroupResult.restore(task_info[1])
        states = [AsyncResult(task_id).state for task_id in task_info['list']]
        prog = sum([s!='PENDING' for s in states])/len(states) * 0.9
        # print(f'{prog*100:.1f}', ''.join([s[0] for s in states]))
        return HttpResponse(f'{prog*100:.1f}')

def get_cloud(request, task_id):
    res = AsyncResult(task_id)
    if not res.ready():
        return JsonResponse({'status': 'pending'})
    else:
        lemmas = sorted(list(res.result['lemmas'].items()), key=lambda x: -x[1])
        lemmas = lemmas[0:200]
        representatives = {k: res.result['representatives'][k] for k in map(lambda x: x[0], lemmas)}
        print(representatives)
        return JsonResponse({'status': 'success', 'data': {'lemmas': lemmas, 'representatives': res.result['representatives']}})

def flatten(xs):
    for x in xs:
        if isinstance(x, tuple) or isinstance(x, list):
            yield from flatten(x)
        else:
            yield x

def create_cloud(request):
    headers={'Authorization': 'Bearer '+request.session['access_token']}
    top = requests.get(SPOTIFY_API_URL + 'me/top/tracks?limit=50&time_range=medium_term', headers=headers).json()
    # top = requests.get(SPOTIFY_API_URL + 'me/top/tracks?limit=10&time_range=medium_term', headers=headers).json()
    if 'error' in top:
        if top['error']['message'] == 'The access token expired':
            request.session.flush()
        else:
            print(top['error']['message'])
        return redirect('..')
    task = process_all_songs(top)
    task_result = task.delay()
    task_result.parent.save()
    ct = CloudTask()
    ct.task_id = task_result.id
    id_chain = []
    node = task_result
    while node:
        id_chain.append(node.id)
        node = node.parent
    id_list = [x for x in flatten(task_result.as_tuple()) if x!=None]
    print(id_list)
    ct.celery_info = json.dumps({'chain': id_chain, 'list': id_list})
    ct.save()
    return redirect('../cloud/' + task_result.id)
