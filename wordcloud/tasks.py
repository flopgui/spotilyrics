
from celery import shared_task, group, current_task, chord
import lyricsgenius
from .song_tokenizer import count_words, tokenize_song_n
from django.conf import settings
# import langid.langid as langidl
import langid
from collections import Counter
import requests
from bs4 import BeautifulSoup


@shared_task
def fetch_song(track):
    url = "http://www.google.com/search?q="+track['name']+"+"+track['artists'][0]['name']+"+lyrics&ie=UTF-8&tob=true"
    t = requests.get(url).text
    t = BeautifulSoup(t, "html.parser")
    try:
        lyrics = t.find("div","hwc").text
    except:
        return None
    return lyrics


@shared_task
def identify_song_language(lyrics):
    # language_identifier = langidl.LanguageIdentifier.from_modelstring(langidl.model, norm_probs=True)
    if lyrics is None:
        return None
    # language = language_identifier.classify(lyrics)
    language = langid.classify(lyrics)
    print(lyrics, language)
    # if language[1] < 0.5:
    #     return None
    # else:
    #     return {'lyrics': lyrics, 'language': language[0]}
    return {'lyrics': lyrics, 'language': language[0]}


@shared_task
def tokenize_song(lyrics):
    if lyrics is None:
        return None
    tok = tokenize_song_n(lyrics['lyrics'], lyrics['language'])
    if tok is None:
        return None
    lemmas = [t.lemma_ for t in tok]
    representatives = {}
    for t in tok:
        if t.lemma_ not in representatives:
            representatives[t.lemma_] = Counter()
        representatives[t.lemma_].update([t.lower_])
    return {'lemmas': lemmas, 'representatives': representatives}

@shared_task
def count_words_task(songs):
    cw = count_words(songs)
    return {'lemmas': cw[0], 'representatives': cw[1]}

#@shared_task
def process_all_songs(songlist):
    songs = []
    tasks = []
    for i, track in enumerate(songlist['items']):
        print(i, track['name'], '-', track['artists'][0]['name'])
        task = (fetch_song.s(track) | identify_song_language.s() | tokenize_song.s())
        tasks.append(task)
    tasks = group(*tasks)
    tasks = (tasks | count_words_task.s())
    return tasks

