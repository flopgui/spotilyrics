from django.db import models
from django.contrib.sessions.models import Session

# class UserSession(models.Model):
#     session = models.OneToOneField(Session, on_delete=models.CASCADE)
#     access_token = models.CharField(max_length=200)
#     refresh_token = models.CharField(max_length=200)
#     user_uri = models.CharField(max_length=200)
#     user_display_name = models.CharField(max_length=200)

class CloudTask(models.Model):
    task_id = models.CharField(max_length=36)
    celery_info = models.JSONField()
