from django.urls import path
from . import views

app_name = 'wordcloud'
urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('spotify_callback/', views.spotify_callback, name='spotify_callback'),
    path('create_cloud/', views.create_cloud, name='create_cloud'),
    path('cloud/<uuid:task_id>', views.cloud, name='cloud'),
    path('cloud_progress/<uuid:task_id>', views.cloud_progress, name='cloud_progress'),
    path('get_cloud/<uuid:task_id>', views.get_cloud, name='get_cloud'),
]
